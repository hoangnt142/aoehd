
(function() {
  var COUNT_FRAMERATE, COUNT_MS_PER_FRAME, DIGIT_FORMAT, DIGIT_HTML, DIGIT_SPEEDBOOST, DURATION, FORMAT_MARK_HTML, FORMAT_PARSER, FRAMERATE, FRAMES_PER_VALUE, MS_PER_FRAME, MutationObserver, Odometer, RIBBON_HTML, TRANSITION_END_EVENTS, TRANSITION_SUPPORT, VALUE_HTML, addClass, createFromHTML, fractionalPart, now, removeClass, requestAnimationFrame, round, transitionCheckStyles, trigger, truncate, wrapJQuery, _jQueryWrapped, _old, _ref, _ref1,
  __slice = [].slice;

  VALUE_HTML = '<span class="odometer-value"></span>';

  RIBBON_HTML = '<span class="odometer-ribbon"><span class="odometer-ribbon-inner">' + VALUE_HTML + '</span></span>';

  DIGIT_HTML = '<span class="odometer-digit"><span class="odometer-digit-spacer">8</span><span class="odometer-digit-inner">' + RIBBON_HTML + '</span></span>';

  FORMAT_MARK_HTML = '<span class="odometer-formatting-mark"></span>';

  DIGIT_FORMAT = '(ddd).dd';

  FORMAT_PARSER = /^\(?([^)]*)\)?(?:(.)(d+))?$/;

  FRAMERATE = 30;

  DURATION = 2000;

  COUNT_FRAMERATE = 20;

  FRAMES_PER_VALUE = 2;

  DIGIT_SPEEDBOOST = .5;

  MS_PER_FRAME = 500 / FRAMERATE;

  COUNT_MS_PER_FRAME = 1000 / COUNT_FRAMERATE;

  TRANSITION_END_EVENTS = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd';

  transitionCheckStyles = document.createElement('div').style;

  TRANSITION_SUPPORT = (transitionCheckStyles.transition != null) || (transitionCheckStyles.webkitTransition != null) || (transitionCheckStyles.mozTransition != null) || (transitionCheckStyles.oTransition != null);

  requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;

  createFromHTML = function(html) {
    var el;
    el = document.createElement('div');
    el.innerHTML = html;
    return el.children[0];
  };

  var customers = JSON.parse("[{\"id\":202,\"name\":\"TRẦN VĂN HAI\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":109,\"meed\":null},{\"id\":203,\"name\":\"VŨ QUANG CƯỜNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":118,\"meed\":null},{\"id\":204,\"name\":\"PHAN CÔNG TÀI\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":115,\"meed\":null},{\"id\":205,\"name\":\"TRẦN THỊ ÁNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":3,\"meed\":null},{\"id\":206,\"name\":\"TRẦN HOÀNG ANH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":119,\"meed\":null},{\"id\":207,\"name\":\"NGUYỄN TRUNG HIẾU\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":84,\"meed\":null},{\"id\":208,\"name\":\"PHAN VĂN HÙNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":60,\"meed\":null},{\"id\":209,\"name\":\"NGUYỄN HỒNG HÀ\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":20,\"meed\":null},{\"id\":210,\"name\":\"NGUYỄN THANH TÙNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":62,\"meed\":null},{\"id\":211,\"name\":\"LÊ VĂN ĐẠO\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":127,\"meed\":null},{\"id\":212,\"name\":\"DƯƠNG QUANG ANH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":125,\"meed\":null},{\"id\":213,\"name\":\"NGUYỄN DUY KHÁNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-04T03:32:45.000Z\",\"marked\":false,\"code\":65,\"meed\":\"giainhat.jpg\"},{\"id\":214,\"name\":\"LÊ KHẮC THẾ\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":111,\"meed\":null},{\"id\":215,\"name\":\"CHỬ QUỲNH PHƯƠNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-04T03:37:46.000Z\",\"marked\":false,\"code\":64,\"meed\":\"giaikhuyenkhich.jpg\"},{\"id\":216,\"name\":\"NGUYỄN DUY KHIÊM\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":108,\"meed\":null},{\"id\":217,\"name\":\"HÀ MINH TUÂN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":134,\"meed\":null},{\"id\":218,\"name\":\"NGUYỄN ĐỨC THỊNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":136,\"meed\":null},{\"id\":219,\"name\":\"LÊ MINH VIỆT\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":86,\"meed\":null},{\"id\":220,\"name\":\"PHẠM ĐỨC KHÁNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":105,\"meed\":null},{\"id\":221,\"name\":\"PHAN VĂN CƯỜNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":90,\"meed\":null},{\"id\":222,\"name\":\"ĐỖ HOÀNG GIANG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":132,\"meed\":null},{\"id\":223,\"name\":\"TRỊNH PHÚ HÙNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-04T03:30:31.000Z\",\"marked\":false,\"code\":137,\"meed\":null},{\"id\":224,\"name\":\"HOÀNG TRẦN VIỆT ĐỨC\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":141,\"meed\":null},{\"id\":225,\"name\":\"CAO VĂN THẮNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":112,\"meed\":null},{\"id\":226,\"name\":\"NGUYỄN MINH ĐỨC\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":37,\"meed\":null},{\"id\":227,\"name\":\"MAI NGUYỄN LỘC\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":73,\"meed\":null},{\"id\":228,\"name\":\"NGUYỄN THỊ KIM THÚY\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":140,\"meed\":null},{\"id\":229,\"name\":\"HOÀNG ĐÌNH THÀNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":76,\"meed\":null},{\"id\":230,\"name\":\"TRẦN BÁ QUÂN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":120,\"meed\":null},{\"id\":231,\"name\":\"ĐỖ ANH TUẤN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":22,\"meed\":null},{\"id\":232,\"name\":\"NGUYỄN VĂN HƯNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":122,\"meed\":null},{\"id\":233,\"name\":\"LƯU TIẾN NHẬT\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":117,\"meed\":null},{\"id\":234,\"name\":\"ĐINH THỊ VIỆT NGA\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":151,\"meed\":null},{\"id\":235,\"name\":\"NGUYỄN TIẾN DŨNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":26,\"meed\":null},{\"id\":236,\"name\":\"NGUYỄN VIẾT THÀNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":99,\"meed\":null},{\"id\":237,\"name\":\"TRẦN THỊ VUI\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":12,\"meed\":null},{\"id\":238,\"name\":\"NGUYỄN VĂN THẮNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":98,\"meed\":null},{\"id\":239,\"name\":\"PHẠM THỊ PHƯƠNG UYÊN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":14,\"meed\":null},{\"id\":240,\"name\":\"TRẦN HOÀI THU\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":94,\"meed\":null},{\"id\":241,\"name\":\"LÊ HOÀNG ANH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":146,\"meed\":null},{\"id\":242,\"name\":\"THÁI THANH TÙNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":113,\"meed\":null},{\"id\":243,\"name\":\"ĐOÀN TRUNG HIẾU\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":1,\"meed\":null},{\"id\":244,\"name\":\"TRẦN LÔ GIANG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":46,\"meed\":null},{\"id\":245,\"name\":\"ĐỖ NAM SƠN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":39,\"meed\":null},{\"id\":246,\"name\":\"PHẠM THỊ NGỌC ANH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":24,\"meed\":null},{\"id\":247,\"name\":\"PHẠM THU HẰNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":89,\"meed\":null},{\"id\":248,\"name\":\"LƯU HỒNG HẠNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":41,\"meed\":null},{\"id\":249,\"name\":\"ĐỖ THỊ THU HƯỜNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":128,\"meed\":null},{\"id\":250,\"name\":\"LÊ THỊ THU HƯƠNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":13,\"meed\":null},{\"id\":251,\"name\":\"NGUYỄN NGỌC THẠCH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":9,\"meed\":null},{\"id\":252,\"name\":\"NGUYỄN THANH ĐỨC\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":131,\"meed\":null},{\"id\":253,\"name\":\"PHÙNG THỊ MỪNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":121,\"meed\":null},{\"id\":254,\"name\":\"PHẠM THỊ VÂN AN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":101,\"meed\":null},{\"id\":255,\"name\":\"TRẦN LÊ HIẾU\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":104,\"meed\":null},{\"id\":256,\"name\":\"PHẠM THỊ THANH HUYỀN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":23,\"meed\":null},{\"id\":257,\"name\":\"PHẠM MẠNH HÀ\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":154,\"meed\":null},{\"id\":258,\"name\":\"TỐNG VĂN HƯNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":6,\"meed\":null},{\"id\":259,\"name\":\"VŨ ANH TUẤN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":55,\"meed\":null},{\"id\":260,\"name\":\"TRƯƠNG THỊ XUÂN MAI\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":25,\"meed\":null},{\"id\":261,\"name\":\"ĐOÀN ANH TUẤN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":34,\"meed\":null},{\"id\":262,\"name\":\"NGUYỄN TRUNG ĐỈNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":68,\"meed\":null},{\"id\":263,\"name\":\"LƯƠNG TUẤN SƠN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-04T03:22:10.000Z\",\"marked\":false,\"code\":81,\"meed\":null},{\"id\":264,\"name\":\"LUYỆN THỊ HỒNG HẠNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":29,\"meed\":null},{\"id\":265,\"name\":\"VŨ ĐỨC PHƯƠNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":72,\"meed\":null},{\"id\":266,\"name\":\"HOÀNG ANH TUẤN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":107,\"meed\":null},{\"id\":267,\"name\":\"NGÔ ĐỨC HUY\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":149,\"meed\":null},{\"id\":268,\"name\":\"NGUYỄN THỊ NHƯ QUỲNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":150,\"meed\":null},{\"id\":269,\"name\":\"NGUYỄN THỊ THANH LOAN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":70,\"meed\":null},{\"id\":270,\"name\":\"ĐÀO ANH THƯ\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":156,\"meed\":null},{\"id\":271,\"name\":\"NGUYỄN LONG BIÊN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":45,\"meed\":null},{\"id\":272,\"name\":\"CAO HÀ VĨNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":147,\"meed\":null},{\"id\":273,\"name\":\"NGUYỄN ĐỨC DUY\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":155,\"meed\":null},{\"id\":274,\"name\":\"PHẠM THỊ LOAN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":69,\"meed\":null},{\"id\":275,\"name\":\"NGUYỄN ĐỨC PHÚ\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:56.000Z\",\"updated_at\":\"2018-07-03T11:49:56.000Z\",\"marked\":false,\"code\":56,\"meed\":null},{\"id\":276,\"name\":\"LÊ HOÀNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":85,\"meed\":null},{\"id\":277,\"name\":\"LÊ CÔNG BIỂN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":110,\"meed\":null},{\"id\":278,\"name\":\"NGUYỄN HỮU CHUNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":5,\"meed\":null},{\"id\":279,\"name\":\"LÊ MẠNH DŨNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":102,\"meed\":null},{\"id\":280,\"name\":\"NGUYỄN NINH ANH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":130,\"meed\":null},{\"id\":281,\"name\":\"HẠ BÁ LONG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":143,\"meed\":null},{\"id\":282,\"name\":\"NGUYỄN DUY KIÊN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":83,\"meed\":null},{\"id\":283,\"name\":\"NGUYỄN ANH ĐỨC\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":78,\"meed\":null},{\"id\":284,\"name\":\"PHẠM ĐỨC DUY\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":100,\"meed\":null},{\"id\":285,\"name\":\"NGUYỄN THỊ MINH NGUYỆT\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":47,\"meed\":null},{\"id\":286,\"name\":\"PHẠM THỊ LAN ANH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":152,\"meed\":null},{\"id\":287,\"name\":\"NGUYỄN PHƯỚC TUẤN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":8,\"meed\":null},{\"id\":288,\"name\":\"HOÀNG NGỌC LINH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":159,\"meed\":null},{\"id\":289,\"name\":\"PHẠM ĐỨC TRUNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":44,\"meed\":null},{\"id\":290,\"name\":\"TRẦN XUÂN TÙNG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":162,\"meed\":null},{\"id\":291,\"name\":\"NGUYỄN TRỌNG ĐIỆP\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":160,\"meed\":null},{\"id\":292,\"name\":\"NGUYỄN VĨNH LỘC\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":158,\"meed\":null},{\"id\":293,\"name\":\"CÔNG ĐỨC HIẾU\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":163,\"meed\":null},{\"id\":294,\"name\":\"NGUYỄN DUY KHÁNH\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":65,\"meed\":null},{\"id\":295,\"name\":\"CÙ ĐỨC HIẾU\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":168,\"meed\":null},{\"id\":296,\"name\":\"NGUYỄN ĐĂNG THỨ\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-04T03:38:07.000Z\",\"marked\":false,\"code\":166,\"meed\":\"giaidacbiet.jpg\"},{\"id\":297,\"name\":\"PHẠM MINH TUYỂN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":169,\"meed\":null},{\"id\":298,\"name\":\"CHU THẾ SƠN\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-04T15:26:42.000Z\",\"marked\":false,\"code\":172,\"meed\":\"giaikhuyenkhich.jpg\"},{\"id\":299,\"name\":\"NGUYỄN THỊ THU NGA\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-04T03:36:59.000Z\",\"marked\":false,\"code\":174,\"meed\":\"giaikhuyenkhich.jpg\"},{\"id\":300,\"name\":\"TRẦN THỊ GIANG\",\"phone\":null,\"created_at\":\"2018-07-03T11:49:57.000Z\",\"updated_at\":\"2018-07-03T11:49:57.000Z\",\"marked\":false,\"code\":170,\"meed\":null}]")

  var spining = false;
  var meed = 'giaikhuyenkhich.jpg';
  var maxTicket = parseInt(document.URL.split("/")[document.URL.split("/").length -1])
  var customer_codes = [109, 118, 115, 3, 119, 84, 60, 20, 62, 127, 125, 65, 111, 64, 108, 134, 136, 86, 105, 90, 132, 137, 141, 112, 37, 73, 140, 76, 120, 22, 122, 117, 151, 26, 99, 12, 98, 14, 94, 146, 113, 1, 46, 39, 24, 89, 41, 128, 13, 9, 131, 121, 101, 104, 23, 154, 6, 55, 25, 34, 68, 81, 29, 72, 107, 149, 150, 70, 156, 45, 147, 155, 69, 56, 85, 110, 5, 102, 130, 143, 83, 78, 100, 47, 152, 8, 159, 44, 162, 160, 158, 163, 65, 168, 166, 169, 172, 174, 170]

  var started = false;

  var sound = new Howl({
    src: ['/audio/sm-roller-loop.mp3'],
  });

  var soundSuccess = new Howl({
    src: ['/audio/success.mp3', '/audio/success2.mp3'],
  });

  removeClass = function(el, name) {
    return el.className = el.className.replace(new RegExp("(^| )" + (name.split(' ').join('|')) + "( |$)", 'gi'), ' ');
  };

  addClass = function(el, name) {
    removeClass(el, name);
    return el.className += " " + name;
  };

  trigger = function(el, name) {
    var evt;
    if (document.createEvent != null) {
      evt = document.createEvent('HTMLEvents');
      evt.initEvent(name, true, true);
      return el.dispatchEvent(evt);
    }
  };

  now = function() {
    var _ref, _ref1;
    return (_ref = (_ref1 = window.performance) != null ? typeof _ref1.now === "function" ? _ref1.now() : void 0 : void 0) != null ? _ref : +(new Date);
  };

  round = function(val, precision) {
    if (precision == null) {
      precision = 0;
    }
    if (!precision) {
      return Math.round(val);
    }
    val *= Math.pow(10, precision);
    val += 0.5;
    val = Math.floor(val);
    return val /= Math.pow(10, precision);
  };

  truncate = function(val) {
    if (val < 0) {
      return Math.ceil(val);
    } else {
      return Math.floor(val);
    }
  };

  fractionalPart = function(val) {
    return val - round(val);
  };

  _jQueryWrapped = false;

  (wrapJQuery = function() {
    var property, _i, _len, _ref, _results;
    if (_jQueryWrapped) {
      return;
    }
    if (window.jQuery != null) {
      _jQueryWrapped = true;
      _ref = ['html', 'text'];
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        property = _ref[_i];
        _results.push((function(property) {
          var old;
          old = window.jQuery.fn[property];
          return window.jQuery.fn[property] = function(val) {
            var _ref1;
            if ((val == null) || (((_ref1 = this[0]) != null ? _ref1.odometer : void 0) == null)) {
              return old.apply(this, arguments);
            }
            return this[0].odometer.update(val);
          };
        })(property));
      }
      return _results;
    }
  })();

  setTimeout(wrapJQuery, 0);

  Odometer = (function() {
    function Odometer(options) {
      var e, k, property, v, _base, _i, _len, _ref, _ref1, _ref2,
        _this = this;
      this.options = options;
      this.el = this.options.el;
      if (this.el.odometer != null) {
        return this.el.odometer;
      }
      this.el.odometer = this;
      _ref = Odometer.options;
      for (k in _ref) {
        v = _ref[k];
        if (this.options[k] == null) {
          this.options[k] = v;
        }
      }
      if ((_base = this.options).duration == null) {
        _base.duration = DURATION;
      }
      this.MAX_VALUES = ((this.options.duration / MS_PER_FRAME) / FRAMES_PER_VALUE) | 0;
      this.resetFormat();
      this.value = this.cleanValue((_ref1 = this.options.value) != null ? _ref1 : '');
      this.renderInside();
      this.render();
      try {
        _ref2 = ['innerHTML', 'innerText', 'textContent'];
        for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
          property = _ref2[_i];
          if (this.el[property] != null) {
            (function(property) {
              return Object.defineProperty(_this.el, property, {
                get: function() {
                  var _ref3;
                  if (property === 'innerHTML') {
                    return _this.inside.outerHTML;
                  } else {
                    return (_ref3 = _this.inside.innerText) != null ? _ref3 : _this.inside.textContent;
                  }
                },
                set: function(val) {
                  return _this.update(val);
                }
              });
            })(property);
          }
        }
      } catch (_error) {
        e = _error;
        this.watchForMutations();
      }
      this;
    }

    Odometer.prototype.renderInside = function() {
      this.inside = document.createElement('div');
      this.inside.className = 'odometer-inside';
      this.el.innerHTML = '';
      return this.el.appendChild(this.inside);
    };

    Odometer.prototype.watchForMutations = function() {
      var e,
        _this = this;
      if (MutationObserver == null) {
        return;
      }
      try {
        if (this.observer == null) {
          this.observer = new MutationObserver(function(mutations) {
            var newVal;
            newVal = _this.el.innerText;
            _this.renderInside();
            _this.render(_this.value);
            return _this.update(newVal);
          });
        }
        this.watchMutations = true;
        return this.startWatchingMutations();
      } catch (_error) {
        e = _error;
      }
    };

    Odometer.prototype.startWatchingMutations = function() {
      if (this.watchMutations) {
        return this.observer.observe(this.el, {
          childList: true
        });
      }
    };

    Odometer.prototype.stopWatchingMutations = function() {
      var _ref;
      return (_ref = this.observer) != null ? _ref.disconnect() : void 0;
    };

    Odometer.prototype.cleanValue = function(val) {
      var _ref;
      if (typeof val === 'string') {
        val = val.replace((_ref = this.format.radix) != null ? _ref : '.', '<radix>');
        val = val.replace(/[.,]/g, '');
        val = val.replace('<radix>', '.');
        val = parseFloat(val, 10) || 0;
      }
      return round(val, this.format.precision);
    };

    Odometer.prototype.bindTransitionEnd = function() {
      var event, renderEnqueued, _i, _len, _ref, _results,
        _this = this;
      if (this.transitionEndBound) {
        return;
      }
      this.transitionEndBound = true;
      renderEnqueued = false;
      _ref = TRANSITION_END_EVENTS.split(' ');
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        event = _ref[_i];
        _results.push(this.el.addEventListener(event, function() {
          if (renderEnqueued) {
            return true;
          }
          renderEnqueued = true;
          setTimeout(function() {
            _this.render();
            renderEnqueued = false;
            return trigger(_this.el, 'odometerdone');
          }, 0);
          return true;
        }, false));
      }
      return _results;
    };

    Odometer.prototype.resetFormat = function() {
      var format, fractional, parsed, precision, radix, repeating, _ref, _ref1;
      format = (_ref = this.options.format) != null ? _ref : DIGIT_FORMAT;
      format || (format = 'd');
      parsed = FORMAT_PARSER.exec(format);
      if (!parsed) {
        throw new Error("Odometer: Unparsable digit format");
      }
      _ref1 = parsed.slice(1, 4), repeating = _ref1[0], radix = _ref1[1], fractional = _ref1[2];
      precision = (fractional != null ? fractional.length : void 0) || 0;
      return this.format = {
        repeating: repeating,
        radix: radix,
        precision: precision
      };
    };

    Odometer.prototype.render = function(value) {
      
      var classes, cls, digit, match, newClasses, theme, wholePart, _i, _j, _len, _len1, _ref;
      if (value == null) {
        value = this.value;
        console.log(value)
        sound.stop()
        if (started) {
          soundSuccess.play()
        }
      }
      this.stopWatchingMutations();
      this.resetFormat();
      this.inside.innerHTML = '';
      theme = this.options.theme;
      classes = this.el.className.split(' ');
      newClasses = [];
      for (_i = 0, _len = classes.length; _i < _len; _i++) {
        cls = classes[_i];
        if (!cls.length) {
          continue;
        }
        if (match = /^odometer-theme-(.+)$/.exec(cls)) {
          theme = match[1];
          continue;
        }
        if (/^odometer(-|$)/.test(cls)) {
          continue;
        }
        newClasses.push(cls);
      }
      newClasses.push('odometer');
      if (!TRANSITION_SUPPORT) {
        newClasses.push('odometer-no-transitions');
      }
      if (theme) {
        newClasses.push("odometer-theme-" + theme);
      } else {
        newClasses.push("odometer-auto-theme");
      }
      this.el.className = newClasses.join(' ');
      this.ribbons = {};
      this.digits = [];
      wholePart = !this.format.precision || !fractionalPart(value) || false;
      _ref = value.toString().split('').reverse();
      for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
        digit = _ref[_j];
        if (digit === '.') {
          wholePart = true;
        }
        this.addDigit(digit, wholePart);
      }
      // return this.startWatchingMutations();
    };

    Odometer.prototype.update = function(newValue) {
      var diff,
        _this = this;
      newValue = this.cleanValue(newValue);
      if (!(diff = newValue - this.value)) {
        return;
      }
      removeClass(this.el, 'odometer-animating-up odometer-animating-down odometer-animating');
      if (diff > 0) {
        addClass(this.el, 'odometer-animating-up');
      } else {
        addClass(this.el, 'odometer-animating-down');
      }
      this.stopWatchingMutations();
      
      this.animate(newValue);
      this.startWatchingMutations();
      setTimeout(function() {
        _this.el.offsetHeight;
        return addClass(_this.el, 'odometer-animating');
      }, 0);
      if (newValue >= 10 && newValue < 100) {
        return this.value = '0' + newValue;
      } else if (newValue < 10) {
        return this.value = '00' + newValue;
      }
      return this.value = newValue;
    };

    Odometer.prototype.renderDigit = function() {
      return createFromHTML(DIGIT_HTML);
    };

    Odometer.prototype.insertDigit = function(digit, before) {
      if (before != null) {
        return this.inside.insertBefore(digit, before);
      } else if (!this.inside.children.length) {
        return this.inside.appendChild(digit);
      } else {
        return this.inside.insertBefore(digit, this.inside.children[0]);
      }
    };

    Odometer.prototype.addSpacer = function(chr, before, extraClasses) {
      var spacer;
      spacer = createFromHTML(FORMAT_MARK_HTML);
      spacer.innerHTML = chr;
      if (extraClasses) {
        addClass(spacer, extraClasses);
      }
      return this.insertDigit(spacer, before);
    };

    Odometer.prototype.addDigit = function(value, repeating) {
      var chr, digit, resetted, _ref;
      if (repeating == null) {
        repeating = true;
      }
      if (value === '-') {
        return this.addSpacer(value, null, 'odometer-negation-mark');
      }
      if (value === '.') {
        return this.addSpacer((_ref = this.format.radix) != null ? _ref : '.', null, 'odometer-radix-mark');
      }
      if (repeating) {
        resetted = false;
        while (true) {
          if (!this.format.repeating.length) {
            if (resetted) {
              throw new Error("Bad odometer format without digits");
            }
            this.resetFormat();
            resetted = true;
          }
          chr = this.format.repeating[this.format.repeating.length - 1];
          this.format.repeating = this.format.repeating.substring(0, this.format.repeating.length - 1);
          if (chr === 'd') {
            break;
          }
          this.addSpacer(chr);
        }
      }
      digit = this.renderDigit();
      digit.querySelector('.odometer-value').innerHTML = value;
      this.digits.push(digit);
      return this.insertDigit(digit);
    };

    Odometer.prototype.animate = function(newValue) {
      return this.animateSlide(newValue);
    };



    Odometer.prototype.getDigitCount = function() {
      var i, max, value, values, _i, _len;
      values = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      for (i = _i = 0, _len = values.length; _i < _len; i = ++_i) {
        value = values[i];
        values[i] = Math.abs(value);
      }
      max = Math.max.apply(Math, values);
      return Math.ceil(Math.log(max + 1) / Math.log(10));
    };

    Odometer.prototype.getFractionalDigitCount = function() {
      var i, parser, parts, value, values, _i, _len;
      values = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      parser = /^\-?\d*\.(\d*?)0*$/;
      for (i = _i = 0, _len = values.length; _i < _len; i = ++_i) {
        value = values[i];
        values[i] = value.toString();
        parts = parser.exec(values[i]);
        if (parts == null) {
          values[i] = 0;
        } else {
          values[i] = parts[1].length;
        }
      }
      return Math.max.apply(Math, values);
    };

    Odometer.prototype.resetDigits = function() {
      this.digits = [];
      this.ribbons = [];
      this.inside.innerHTML = '';
      return this.resetFormat();
    };

    Odometer.prototype.animateSlide = function(newValue) {
      var boosted, cur, diff, digitCount, digits, dist, end, fractionalCount, frame, frames, i, incr, j, mark, numEl, oldValue, start, _base, _i, _j, _k, _l, _len, _len1, _len2, _m, _ref, _results;
      oldValue = this.value;
      fractionalCount = this.getFractionalDigitCount(oldValue, newValue);
      if (fractionalCount) {
        newValue = newValue * Math.pow(10, fractionalCount);
        oldValue = oldValue * Math.pow(10, fractionalCount);
      }
      if (!(diff = newValue - oldValue)) {
        return;
      }
      this.bindTransitionEnd();
      digitCount = this.getDigitCount(oldValue, newValue);
      digits = [];
      boosted = 0;
      for (i = _i = 0; 0 <= digitCount ? _i < digitCount : _i > digitCount; i = 0 <= digitCount ? ++_i : --_i) {
        start = truncate(oldValue / Math.pow(10, digitCount - i - 1));
        end = truncate(newValue / Math.pow(10, digitCount - i - 1));
        dist = end - start;
        if (Math.abs(dist) > this.MAX_VALUES) {
          frames = [];
          incr = dist / (this.MAX_VALUES + this.MAX_VALUES * boosted * DIGIT_SPEEDBOOST);
          cur = start;
          while ((dist > 0 && cur < end) || (dist < 0 && cur > end)) {
            frames.push(Math.round(cur));
            cur += incr;
          }
          if (frames[frames.length - 1] !== end) {
            frames.push(end);
          }
          boosted++;
        } else {
          frames = (function() {
            _results = [];
            for (var _j = start; start <= end ? _j <= end : _j >= end; start <= end ? _j++ : _j--){ _results.push(_j); }
            return _results;
          }).apply(this);
        }
        for (i = _k = 0, _len = frames.length; _k < _len; i = ++_k) {
          frame = frames[i];
          frames[i] = Math.abs(frame % 10);
        }
        digits.push(frames);
      }
      this.resetDigits();
      _ref = digits.reverse();
      for (i = _l = 0, _len1 = _ref.length; _l < _len1; i = ++_l) {
        frames = _ref[i];
        if (!this.digits[i]) {
          this.addDigit(' ', i >= fractionalCount);
        }
        if ((_base = this.ribbons)[i] == null) {
          _base[i] = this.digits[i].querySelector('.odometer-ribbon-inner');
        }
        this.ribbons[i].innerHTML = '';
        if (diff < 0) {
          frames = frames.reverse();
        }
        for (j = _m = 0, _len2 = frames.length; _m < _len2; j = ++_m) {
          frame = frames[j];
          numEl = document.createElement('div');
          numEl.className = 'odometer-value';
          numEl.innerHTML = frame;
          this.ribbons[i].appendChild(numEl);
          if (j === frames.length - 1) {
            addClass(numEl, 'odometer-last-value');
          }
          if (j === 0) {
            addClass(numEl, 'odometer-first-value');
          }
        }
      }
      if (start < 0) {
        this.addDigit('-');
      }
      mark = this.inside.querySelector('.odometer-radix-mark');
      if (mark != null) {
        mark.parent.removeChild(mark);
      }
      if (fractionalCount) {
        return this.addSpacer(this.format.radix, this.digits[fractionalCount - 1], 'odometer-radix-mark');
      }
    };

    return Odometer;

  })();

  Odometer.options = (_ref = window.odometerOptions) != null ? _ref : {};

  setTimeout(function() {
    var k, v, _base, _ref1, _results;
    if (window.odometerOptions) {
      _ref1 = window.odometerOptions;
      _results = [];
      for (k in _ref1) {
        v = _ref1[k];
        _results.push((_base = Odometer.options)[k] != null ? (_base = Odometer.options)[k] : _base[k] = v);
      }
      return _results;
    }
  }, 0);

  Odometer.init = function() {
    var el, elements, _i, _len, _ref1, _results;
    if (document.querySelectorAll == null) {
      return;
    }
    elements = document.querySelectorAll(Odometer.options.selector || '.odometer');
    _results = [];
    for (_i = 0, _len = elements.length; _i < _len; _i++) {
      el = elements[_i];
      _results.push(el.odometer = new Odometer({
        el: el,
        value: (_ref1 = el.innerText) != null ? _ref1 : el.textContent
      }));
    }
    return _results;
  };

  if ((((_ref1 = document.documentElement) != null ? _ref1.doScroll : void 0) != null) && (document.createEventObject != null)) {
    _old = document.onreadystatechange;
    document.onreadystatechange = function() {
      if (document.readyState === 'complete' && Odometer.options.auto !== false) {
        Odometer.init();
      }
      return _old != null ? _old.apply(this, arguments) : void 0;
    };
  } else {
    document.addEventListener('DOMContentLoaded', function() {
      if (Odometer.options.auto !== false) {
        return Odometer.init();
      }
    }, false);
  }

  if (typeof define === 'function' && define.amd) {
    define(['jquery'], function() {
      return Odometer;
    });
  } else if (typeof exports === !'undefined') {
    module.exports = Odometer;
  } else {
    window.Odometer = Odometer;
  }
  
  $(function() {
    var code, doSomeThing, exampleOdometer, exampleOdometerValue, spin, start;
    spin = void 0;
    start = false;
    code = null;
    var showIphone = 'none';
    var show5000 = 'none';
    var limit = 0;
    var clicked = false;
    var targetCode = undefined;
    exampleOdometerValue = 888;
    exampleOdometer = new Odometer({
      el: $('.odometer-example')[0],
      theme: 'car',
      value: exampleOdometerValue
    });
    exampleOdometer.render();
    doSomeThing = function() {
      limit++;
      if (limit == 4) {
        code = customer_codes.random()
        console.log('ket thuc 1', code)
        clearInterval(spin);
      } else {
        code = Math.floor(Math.random() * (999 - 0)).toString();
      }
      return exampleOdometer.update(code);
    };
    start = function() {
      spining = true
      // var sound = new Howl({
      //   src: ['/audio/sm-roller-loop.mp3'],
      //   autoplay: true,
      //   loop: true,
      //   onend: function() {
      //     console.log('Finished!');
      //   }
      // });

      started = true
      sound.play()

      $('.result').hide();
      doSomeThing();
      spin = setInterval((function() {
        return doSomeThing();
      }), 2000);
      setTimeout(function(){
        console.log('ket thuc 2', code, meed)
        customer = customers.find(customer => customer.code == code)
        $('.name').html(customer.name);
        $('.meed-image').attr('src', meed)
        $('.result').css('display', 'flex');
        
        limit = 0;
        console.log('code', code)
        var code_index = customer_codes.indexOf(code);
        if (code_index !== -1) customer_codes.splice(code_index, 1);
        console.log(customer_codes.length)
        code = undefined;
      }, 10000);
    };
    $(document).keyup(function(e) {
      console.log(e.keyCode)

      if (e.keyCode === 32) {
        if (!spining) {
          start()
        }
      }

      // 0 dac biet
      if (e.keyCode === 48) {
        if (!spining) {
          meed = 'giaidacbiet.jpg'
          start()
        }
      }

      // 1 Giai Nhat
      if (e.keyCode === 49) {
        if (!spining) {
          meed = 'giainhat.jpg'
          start()
        }
      }
      // 2 Giai Nhi
      if (e.keyCode === 50) {
        if (!spining) {
          meed = 'giainhi.jpg'
          start()
        }
      }
      // Giai 3
      if (e.keyCode === 51) {
        if (!spining) {
          meed = 'giai3.jpg'
          start()
        }
      }
      // 1 Giai 4
      if (e.keyCode === 52) {
        if (!spining) {
          meed = 'giaikhuyenkhich.jpg'
          start()
        }
      }

      if (e.keyCode === 13) {
        targetCode = undefined;
        code = undefined
        limit = 0;
        showIphone = 'none';
        show5000 = 'none';
        spining = false;
        exampleOdometer.render(888)
        $('.result').hide();
        return;
      }
    });
  })

}).call(this);
Array.prototype.random = function () {
  return this[Math.floor((Math.random()*this.length))];
}