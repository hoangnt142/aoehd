class AddMarkedToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :marked, :boolean, default: false
  end
end
