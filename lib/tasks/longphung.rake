require 'csv'
namespace :longphung do
  task create: :environment do
    GiftCode.delete_all
    Customer.delete_all
    CSV.foreach("#{Rails.root}/backup/longphung.csv").with_index(1) do |row, i|
      customer = Customer.create(
        name: row.last.upcase,
        code: row.first.to_i
      )
      puts "---#{row.first}---#{row.last}"
    end

  end
end
